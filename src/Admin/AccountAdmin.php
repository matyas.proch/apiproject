<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class AccountAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email', null, ['label' => 'Email'])
            ->add('firstName', null, ['label' => 'Jméno'])
            ->add('lastName', null, ['label' => 'Příjmení']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['label' => 'ID'])
            ->addIdentifier('email', null, ['label' => 'Email'])
            ->add('firstName', null, ['label' => 'Jméno'])
            ->add('lastName', null, ['label' => 'Příjmení'])
            ->add('registeredAt', null, ['label' => 'Registrován'])
            ->add('_action', null, [
                'label' => 'Akce',
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->add('id', null, ['label' => 'ID'])
            ->add('email', null, ['label' => 'Email'])
            ->add('firstName', null, ['label' => 'Jméno'])
            ->add('lastName', null, ['label' => 'Příjmení'])
            ->add('registeredAt', null, ['label' => 'Registrován']);
    }
}
