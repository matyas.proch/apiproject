<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Exception thrown when a entity model is not valid. Contains all the errors in array of strings.
 * Should be 400.
 */
class SymfonyValidationException extends BaseException
{
    /**
     * ValidationErrorException constructor.
     *
     * @param ConstraintViolationListInterface $validationErrors
     * @param array                            $debugParameters
     */
    public function __construct(ConstraintViolationListInterface $validationErrors, array $debugParameters = [])
    {
        $parameters = [];

        foreach ($validationErrors as $validationError) {
            $parameters[$validationError->getPropertyPath()] = $validationError->getMessage();
        }

        parent::__construct(
            'Validation errors',
            'validation_errors',
            400,
            $parameters,
            $debugParameters
        );
    }
}
