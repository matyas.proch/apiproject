<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * Exception thrown when account not logged in or session expired.
 * Should be 401.
 */
class UnauthorizedException extends BaseException
{
    /**
     * AccessDeniedException constructor.
     *
     * @param array $parameters
     * @param array $debugParameters
     */
    public function __construct(array $parameters = [], array $debugParameters = [])
    {
        parent::__construct(
            'Request is unauthorized. Not logged in or expired.',
            'unauthorized',
            401,
            $parameters,
            $debugParameters
        );
    }
}
