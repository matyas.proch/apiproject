<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * Exception thrown when data are not found.
 * Should be 404.
 */
class DataNotFoundException extends BaseException
{
    /**
     * DataNotFoundException constructor.
     *
     * @param array $parameters
     * @param array $debugParameters
     */
    public function __construct(array $parameters = [], array $debugParameters = [])
    {
        parent::__construct(
            'Data not found',
            'data_not_found',
            404,
            $parameters,
            $debugParameters
        );
    }
}
