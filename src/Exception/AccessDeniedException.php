<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * Exception thrown when account has no right to an action.
 * Should be 403.
 */
class AccessDeniedException extends BaseException
{
    /**
     * AccessDeniedException constructor.
     *
     * @param array $parameters
     * @param array $debugParameters
     */
    public function __construct(array $parameters = [], array $debugParameters = [])
    {
        parent::__construct(
            'Access denied',
            'access_denied',
            403,
            $parameters,
            $debugParameters
        );
    }
}
