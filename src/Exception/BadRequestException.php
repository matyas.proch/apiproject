<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * Exception thrown when received data are not valid or well-formed.
 * Should be 400.
 */
class BadRequestException extends BaseException
{
    /**
     * BadRequestException constructor.
     *
     * @param array $parameters
     * @param array $debugParameters
     */
    public function __construct(array $parameters = [], array $debugParameters = [])
    {
        parent::__construct(
            'Bad request',
            'bad_request',
            400,
            $parameters,
            $debugParameters
        );
    }
}
