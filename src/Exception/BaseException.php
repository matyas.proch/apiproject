<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

class BaseException extends Exception
{
    /** @var array */
    private $parameters;

    /** @var array */
    private $debugParameters;

    /** @var string */
    private $identification;

    /**
     * BaseException constructor.
     *
     * @param string $publicMessage
     * @param int    $code
     * @param string $identification
     * @param array  $parameters
     * @param array  $debugParameters
     */
    public function __construct(
        string $publicMessage,
        string $identification,
        int $code,
        array $parameters,
        array $debugParameters
    ) {
        $this->identification = $identification;
        $this->parameters = $parameters;
        $this->debugParameters = $debugParameters;

        parent::__construct($publicMessage, $code);
    }

    public function getIdentification(): string
    {
        return $this->identification;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getDebugParameters(): array
    {
        return $this->debugParameters;
    }
}
