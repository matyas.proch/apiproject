<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * Exception thrown when an action is not possible due to some conflicts.
 * Should be 409.
 */
class ConflictException extends BaseException
{
    /**
     * ConflictException constructor.
     *
     * @param array $parameters
     * @param array $debugParameters
     */
    public function __construct(array $parameters = [], array $debugParameters = [])
    {
        parent::__construct(
            'Conflict',
            'conflict',
            409,
            $parameters,
            $debugParameters
        );
    }
}
