<?php

declare(strict_types=1);

namespace App\Data;

interface TransformerPasswordInterface
{
    /**
     * Create new entity from model.
     *
     * @param object $model
     *
     * @return object
     */
    public function populateEntityPassword(object $model): object;
}
