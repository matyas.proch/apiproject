<?php

declare(strict_types=1);

namespace App\Data\Transformer;

use App\Data\Entity\Account;
use App\Data\Model\AccountModel;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountTransformer
{
    /** @var UserPasswordEncoderInterface */
    private $userPasswordEncoder;

    /**
     * AccountTransformer constructor.
     *
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * Create new Account entity.
     *
     * @param AccountModel $model
     *
     * @return Account
     *
     * @throws Exception
     */
    public function populateEntityCreate(AccountModel $model)
    {
        $account = new Account(
            $model->email,
            $model->firstName,
            $model->lastName
        );

        $account->setPassword($this->userPasswordEncoder->encodePassword($account, $model->plainPassword));

        return $account;
    }

    /**
     * Edit Account entity.
     *
     * @param AccountModel $model
     * @param Account      $account
     */
    public function populateEntityEdit(AccountModel $model, Account &$account)
    {
        $account->setFirstName($model->firstName);
        $account->setLastName($model->lastName);
    }

    /**
     * Change password for Account entity.
     *
     * @param AccountModel $model
     * @param Account      $account
     */
    public function populateEntityPassword(AccountModel $model, Account &$account)
    {
        $account->setPassword($this->userPasswordEncoder->encodePassword($account, $model->plainPassword));
    }

    public function populateModelFromArray(array $data): AccountModel
    {
        $accountModel = new AccountModel();

        $accountModel->email = $data['email'] ?? null;
        $accountModel->plainPassword = $data['plain_password'] ?? null;
        $accountModel->firstName = $data['first_name'] ?? null;
        $accountModel->lastName = $data['last_name'] ?? null;

        return $accountModel;
    }
}
