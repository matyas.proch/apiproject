<?php

declare(strict_types=1);

namespace App\Data\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class ApiToken
{
    const EXPIRES_IN = 5184000; // 60 days

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Account
     * @ORM\ManyToOne(targetEntity="App\Data\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"api_token_with_account"})
     */
    protected $account;

    /**
     * @var string
     * @ORM\Column(type="string", length=200)
     */
    protected $token;

    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    protected $expiresAt;

    /**
     * ApiToken constructor.
     *
     * @param Account     $account
     * @param string|null $token
     *
     * @throws Exception
     */
    public function __construct(Account $account, string $token = null)
    {
        $this->account = $account;

        $this->token = $token ?? md5(random_bytes(100)) . md5(random_bytes(100));

        $this->expiresAt = new DateTime('+' . self::EXPIRES_IN . 'seconds');
        $this->createdAt = new Datetime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getExpiresAt(): DateTimeInterface
    {
        return $this->expiresAt;
    }
}
