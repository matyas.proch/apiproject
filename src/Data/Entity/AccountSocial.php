<?php

declare(strict_types=1);

namespace App\Data\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *         name="account_provider_uq",
 *         columns={"account_id", "login_provider"}
 *     ), @ORM\UniqueConstraint(
 *         name="provider_key_uq",
 *         columns={"login_provider", "login_key"}
 *     )}
 * )
 */
class AccountSocial
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Account
     * @ORM\OneToOne(targetEntity="App\Data\Entity\Account", inversedBy="account", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account;

    /**
     * @var LoginProvider
     * @ORM\ManyToOne(targetEntity="App\Data\Entity\LoginProvider")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $loginProvider;

    /**
     * @var string
     * @ORM\Column(type="string", length=200)
     */
    protected $loginKey;

    /**
     * Account constructor.
     *
     * @param Account       $account
     * @param LoginProvider $loginProvider
     * @param string        $loginKey
     */
    public function __construct(Account $account, LoginProvider $loginProvider, string $loginKey)
    {
        $this->account = $account;
        $this->loginProvider = $loginProvider;
        $this->loginKey = $loginKey;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getLoginProvider(): LoginProvider
    {
        return $this->loginProvider;
    }

    public function getLoginKey(): string
    {
        return $this->loginKey;
    }
}
