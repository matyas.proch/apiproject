<?php

declare(strict_types=1);

namespace App\Data\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity()
 */
class PasswordForget
{
    const EXPIRES_IN = 86400;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Account
     * @ORM\ManyToOne(targetEntity="App\Data\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    private $hash;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $used = false;

    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $expiresAt;

    /**
     * PasswordForget constructor.
     *
     * @param Account $account
     * @param string  $hash
     *
     * @throws Exception
     */
    public function __construct(Account $account, string $hash)
    {
        $this->account = $account;
        $this->hash = $hash;
        $this->expiresAt = new DateTime('+' . self::EXPIRES_IN . ' seconds');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function isUsed(): bool
    {
        return $this->used;
    }

    public function setUsed(bool $used): self
    {
        $this->used = $used;

        return $this;
    }

    public function getExpiresAt(): DateTimeInterface
    {
        return $this->expiresAt;
    }
}
