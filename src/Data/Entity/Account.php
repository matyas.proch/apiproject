<?php

declare(strict_types=1);

namespace App\Data\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity()
 */
class Account implements UserInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var ?string
     * @ORM\Column(type="string", length=100, unique=true, nullable=true)
     * @Groups({"account_with_contact"})
     */
    protected $email;

    /**
     * @var ?string
     * @ORM\Column(type="string", length=200, nullable=true)
     * @Groups({"account_with_password"})
     */
    protected $password;

    /**
     * @var string[]
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    protected $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    protected $lastName;

    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    protected $registeredAt;

    /**
     * Account constructor.
     *
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     *
     * @throws Exception
     */
    public function __construct(?string $email, string $firstName, string $lastName)
    {
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;

        $this->roles[] = 'ROLE_REGISTERED';
        $this->registeredAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getUsername(): string
    {
        return (string) $this->id;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function addRole(string $role): self
    {
        $this->roles[] = $role;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getRegisteredAt(): DateTimeInterface
    {
        return $this->registeredAt;
    }

    /*
     * Security things
     */

    public function getSalt(): string
    {
        return (string) $_ENV['PASSWORD_SALT'];
    }

    public function eraseCredentials()
    {
        // Does not contain password in plain form, so its not needed
    }
}
