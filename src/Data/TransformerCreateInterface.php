<?php

declare(strict_types=1);

namespace App\Data;

interface TransformerCreateInterface
{
    /**
     * Create new entity from model.
     *
     * @param object $model
     *
     * @return object
     */
    public function populateEntityCreate(object $model): object;

    /**
     * Fill model with data from array.
     *
     * @param array $data
     *
     * @return object
     */
    public function populateModelFromArray(array $data): object;
}
