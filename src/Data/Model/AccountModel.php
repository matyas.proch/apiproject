<?php

declare(strict_types=1);

namespace App\Data\Model;

use Symfony\Component\Validator\Constraints as Assert;

class AccountModel
{
    /**
     * @Assert\NotBlank(
     *     message="An email was not provied",
     *     groups={"create"})
     * @Assert\Email(
     *     message="Provided email was in a bad format",
     *     groups={"create"})
     */
    public $email;

    /**
     * @Assert\NotBlank(
     *     message="A password was not provied",
     *     groups={"create", "password"})
     */
    public $plainPassword;

    /**
     * @Assert\NotBlank(
     *     message="A first name was not provided",
     *     groups={"create", "edit"})
     * @Assert\Length(
     *     max=100,
     *     maxMessage="The first name can be up to 100 chars long",
     *     groups={"create", "edit"})
     */
    public $firstName;

    /**
     * @Assert\NotBlank(
     *     message="A last name was not provided",
     *     groups={"create", "edit"})
     * @Assert\Length(
     *     max=100,
     *     maxMessage="The last name can be up to 100 chars long",
     *     groups={"create", "edit"})
     */
    public $lastName;
}
