<?php

declare(strict_types=1);

namespace App\Data;

interface TransformerInterface
{
    /**
     * Create new entity from model.
     *
     * @param object $model
     *
     * @return object
     */
    public function populateEntityCreate(object $model): object;

    /**
     * Update entity from model.
     *
     * @param object $model
     * @param object $entity
     *
     * @return object
     */
    public function populateEntityEdit(object $model, object $entity): object;

    /**
     * Fill model with data from array.
     *
     * @param array $data
     *
     * @return object
     */
    public function populateModelFromArray(array $data): object;
}
