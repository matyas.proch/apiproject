<?php

declare(strict_types=1);

namespace App\Business\Service;

use App\Data\Entity\Account;
use App\Data\Transformer\AccountTransformer;
use App\Exception\SymfonyValidationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AccountService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Security */
    private $security;

    /** @var ValidatorInterface */
    private $validator;

    /** @var AccountTransformer */
    private $accountTransformer;

    public function __construct(
        EntityManagerInterface $entityManager,
        Security $security,
        ValidatorInterface $validator,
        AccountTransformer $transformer
    ) {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->validator = $validator;
        $this->accountTransformer = $transformer;
    }

    /**
     * @param array $data
     *
     * @throws SymfonyValidationException
     * @throws Exception
     */
    public function edit(array $data): void
    {
        $accountModel = $this->accountTransformer->populateModelFromArray($data);

        $validationErrors = $this->validator->validate($accountModel, null, ['edit']);

        if (count($validationErrors) > 0) {
            throw new SymfonyValidationException($validationErrors);
        }

        /** @var Account $account */
        $account = $this->security->getUser();

        $this->accountTransformer->populateEntityEdit($accountModel, $account);
        $this->entityManager->merge($account);
        $this->entityManager->flush();
    }
}
