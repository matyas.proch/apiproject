<?php

declare(strict_types=1);

namespace App\Business\Security;

use App\Data\Entity\Account;
use App\Data\Entity\PasswordForget;
use App\Data\Transformer\AccountTransformer;
use App\Exception\BadRequestException;
use App\Exception\ConflictException;
use App\Exception\DataNotFoundException;
use App\Exception\SymfonyValidationException;
use App\Util\Random;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PasswordForgetService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var AccountTransformer */
    private $accountTransformer;

    /** @var ValidatorInterface */
    private $validator;

    /**
     * PasswordForgetService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param AccountTransformer     $accountTransformer
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        AccountTransformer $accountTransformer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->accountTransformer = $accountTransformer;
        $this->validator = $validator;
    }

    /**
     * @param string $email
     *
     * @throws DataNotFoundException
     * @throws Exception
     */
    public function sendResetMail(?string $email): void
    {
        if (null === $email) {
            throw new BadRequestException(['Email was not provided.']);
        }

        /** @var Account|null $account */
        $account = $this->entityManager->getRepository(Account::class)
            ->findOneBy(['email' => $email]);

        if (null === $account) {
            throw new DataNotFoundException(['Account with that email does not exist.']);
        }

        while (true) {
            $hash = Random::string(20);
            $passwordForget = new PasswordForget($account, $hash);

            try {
                $this->entityManager->persist($passwordForget);
                $this->entityManager->flush();
            } catch (UniqueConstraintViolationException $e) {
                continue;
            }

            break;
        }

        //TODO send email;
    }

    /**
     * @param array $data
     *
     * @throws BadRequestException
     * @throws DataNotFoundException
     * @throws ConflictException
     * @throws SymfonyValidationException
     */
    public function setPassword(array $data): void
    {
        $missingValues = [];

        if (!isset($data['hash'])) {
            $missingValues[] = 'Hash not provided';
        }

        if (!isset($data['new_password'])) {
            $missingValues[] = 'New password not provided';
        }

        if (count($missingValues) > 0) {
            throw new BadRequestException($missingValues);
        }

        /** @var PasswordForget|null $passwordForget */
        $passwordForget = $this->entityManager->getRepository(PasswordForget::class)
            ->findOneBy(['hash' => $data['hash']]);

        if (null === $passwordForget) {
            throw new DataNotFoundException(['Hash not found.']);
        }

        if ($passwordForget->isUsed()) {
            throw new ConflictException(['Password was already changed.']);
        }

        if ($passwordForget->getExpiresAt() < new DateTime()) {
            throw new ConflictException(['Link has already expired.']);
        }

        $accountModel = $this->accountTransformer->populateModelFromArray(
            ['plain_password' => $data['new_password']]
        );

        $validationErrors = $this->validator->validate($accountModel, null, ['password']);

        if (count($validationErrors) > 0) {
            throw new SymfonyValidationException($validationErrors);
        }

        $passwordForget->setUsed(true);
        $this->entityManager->merge($passwordForget);

        $account = $passwordForget->getAccount();

        $this->accountTransformer->populateEntityPassword($accountModel, $account);
        $this->entityManager->merge($account);
        $this->entityManager->flush();
    }
}
