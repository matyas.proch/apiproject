<?php

declare(strict_types=1);

namespace App\Business\Security\OAuth2;

use App\Data\Entity\Account;

interface OAuth2ServiceInterface
{
    /**
     * Method to handle the request to authenticate and return the token or bad request.
     *
     * @param array $rawCredentials
     *
     * @return array
     */
    public function auth(array $rawCredentials): array;

    /**
     * Extract the credentials from the request.
     *
     * @param array $rawCredentials
     *
     * @return array
     */
    public function getCredentials(array $rawCredentials): array;

    /**
     * Fetch somehow the user with the credentials.
     *
     * @param array $credentials
     *
     * @return Account
     */
    public function getAccount(array $credentials): Account;

    /**
     * Check if the credentials are right for the user.
     *
     * @param array   $credentials
     * @param Account $account
     */
    public function checkCredentials(array $credentials, Account $account): void;

    /**
     * Get somehow the token for api authentication and return it.
     *
     * @param Account $account
     *
     * @return array
     */
    public function authenticationSuccess(Account $account): array;
}
