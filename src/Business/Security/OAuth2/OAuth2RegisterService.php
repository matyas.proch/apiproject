<?php

declare(strict_types=1);

namespace App\Business\Security\OAuth2;

use App\Data\Entity\ApiToken;
use App\Data\Entity\RefreshToken;
use App\Data\Transformer\AccountTransformer;
use App\Exception\ConflictException;
use App\Exception\SymfonyValidationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OAuth2RegisterService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ValidatorInterface */
    private $validator;

    /** @var AccountTransformer */
    private $accountTransformer;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        AccountTransformer $transformer,
        SerializerInterface $serializer
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->accountTransformer = $transformer;
        $this->serializer = $serializer;
    }

    /**
     * @param array $data
     *
     * @return array
     *
     * @throws ConflictException
     * @throws SymfonyValidationException
     * @throws Exception
     */
    public function createAccount(array $data): array
    {
        $accountModel = $this->accountTransformer->populateModelFromArray($data);

        $validationErrors = $this->validator->validate($accountModel, null, ['create']);

        if (count($validationErrors) > 0) {
            throw new SymfonyValidationException($validationErrors);
        }

        try {
            $account = $this->accountTransformer->populateEntityCreate($accountModel);
            $this->entityManager->persist($account);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictException(['Email is already used']);
        }

        $apiToken = new ApiToken($account);
        $this->entityManager->persist($apiToken);

        $refreshToken = new RefreshToken($account);
        $this->entityManager->persist($refreshToken);
        $this->entityManager->flush();

        /** @var SerializationContext $groups */
        $groups = SerializationContext::create()
            ->setGroups(
                ['Default', 'account_with_contact', 'account_with_personal_info']
            );

        $accountArray = json_decode(
            $this->serializer->serialize(
                $account,
                'json',
                $groups
            ),
            true
        );

        return [
            'access_token' => $apiToken->getToken(),
            'refresh_token' => $refreshToken->getToken(),
            'expires_in' => ApiToken::EXPIRES_IN,
            'token_type' => 'bearer',
            'account' => $accountArray,
        ];
    }
}
