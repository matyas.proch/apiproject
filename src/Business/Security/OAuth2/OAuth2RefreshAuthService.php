<?php

declare(strict_types=1);

namespace App\Business\Security\OAuth2;

use App\Data\Entity\Account;
use App\Data\Entity\ApiToken;
use App\Data\Entity\RefreshToken;
use App\Exception\AccessDeniedException;
use App\Exception\BadRequestException;
use App\Exception\DataNotFoundException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class OAuth2RefreshAuthService implements OAuth2ServiceInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Called to get access token and refresh token according to OAuth2. Refresh token is used to do that.
     *
     * @param array $rawCredentials
     *
     * @return array
     *
     * @throws AccessDeniedException
     * @throws BadRequestException
     * @throws DataNotFoundException
     * @throws Exception
     */
    public function auth(array $rawCredentials): array
    {
        $credentials = $this->getCredentials($rawCredentials);

        /** @var Account $user */
        $user = $this->getAccount($credentials);

        $this->checkCredentials($credentials, $user);

        return $this->authenticationSuccess($user);
    }

    /**
     * Return whatever credentials you want to be passed to getUser() as $credentials.
     *
     * @param array $rawCredentials
     *
     * @return array
     *
     * @throws BadRequestException
     */
    public function getCredentials(array $rawCredentials): array
    {
        $missingCredentials = [];

        if (!isset($rawCredentials['refresh_token'])) {
            $missingCredentials[] = 'Refresh token not provided';
        }

        if (!isset($rawCredentials['client_id'])) {
            $missingCredentials[] = 'Client id not provided';
        }

        if (!isset($rawCredentials['grant_type'])) {
            $missingCredentials[] = 'Grant type not provided';
        } elseif ('refresh_token' !== $rawCredentials['grant_type']) {
            $missingCredentials[] = 'Grant type should be password';
        }

        if (count($missingCredentials) > 0) {
            throw new BadRequestException($missingCredentials);
        }

        return [
            'refresh_token' => $rawCredentials['refresh_token'],
        ];
    }

    /**
     * @param array $credentials
     *
     * @return Account
     *
     * @throws AccessDeniedException
     * @throws DataNotFoundException
     */
    public function getAccount(array $credentials): Account
    {
        /** @var RefreshToken|null $refreshToken */
        $refreshToken = $this->entityManager->getRepository(RefreshToken::class)->findOneBy(['token' => $credentials['refresh_token']]);

        if (null === $refreshToken) {
            throw new DataNotFoundException(['Refresh token could not be found.']);
        }

        if ($refreshToken->getExpiresAt() < new DateTime('now')) {
            throw new AccessDeniedException(['Token expired.']);
        }

        /** @var Account $user */
        $user = $refreshToken->getAccount();
        $this->entityManager->remove($refreshToken);

        return $user;
    }

    /**
     * @param array   $credentials
     * @param Account $account
     */
    public function checkCredentials(array $credentials, Account $account): void
    {
        return;
    }

    /**
     * @param Account $account
     *
     * @return array
     *
     * @throws Exception
     */
    public function authenticationSuccess(Account $account): array
    {
        $apiToken = new ApiToken($account);
        $this->entityManager->persist($apiToken);

        $refreshToken = new RefreshToken($account);
        $this->entityManager->persist($refreshToken);
        $this->entityManager->flush();

        return [
            'access_token' => $apiToken->getToken(),
            'refresh_token' => $refreshToken->getToken(),
            'expires_in' => ApiToken::EXPIRES_IN,
            'token_type' => 'bearer',
        ];
    }
}
