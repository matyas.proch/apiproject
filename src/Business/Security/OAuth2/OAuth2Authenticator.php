<?php

declare(strict_types=1);

namespace App\Business\Security\OAuth2;

use App\Data\Entity\ApiToken;
use App\Exception\UnauthorizedException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\AuthenticationExpiredException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class OAuth2Authenticator extends AbstractGuardAuthenticator
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     *
     * @param Request $request
     *
     * @return bool
     *
     * @throws UnauthorizedException
     */
    public function supports(Request $request)
    {
        if (!$request->headers->has('Authorization')) {
            throw new UnauthorizedException(['Authorization header was not provided']);
        }

        return true;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws UnauthorizedException
     */
    public function getCredentials(Request $request)
    {
        /** @var string $authorization */
        $authorization = $request->headers->get('Authorization', '');
        $authorizationParts = explode(' ', $authorization);

        if (!(2 === count($authorizationParts) && 0 === strcasecmp($authorizationParts[0], 'Bearer'))) {
            throw new UnauthorizedException(['Credentials are not provided correctly.']);
        }

        return [
            'token' => $authorizationParts[1],
        ];
    }

    /**
     * @param array                 $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return UserInterface
     *
     * @throws AuthenticationExpiredException|Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = $credentials['token'];

        /** @var ApiToken|null $apiToken */
        $apiToken = $this->entityManager->getRepository(ApiToken::class)
            ->findOneBy(['token' => $token]);

        if (null === $apiToken) {
            throw new UnauthorizedException(['Token not found.']);
        }

        if ($apiToken->getExpiresAt() < new DateTime('now')) {
            throw new UnauthorizedException(['Token expired.']);
        }

        return $apiToken->getAccount();
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response|void|null
     *
     * @throws UnauthorizedException
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new UnauthorizedException(['Authentication Required.']);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     *
     * @param Request                      $request
     * @param AuthenticationException|null $authException
     *
     * @throws UnauthorizedException
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new UnauthorizedException(['Authentication Required.']);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
