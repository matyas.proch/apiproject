<?php

declare(strict_types=1);

namespace App\Business\Security\OAuth2;

use App\Data\Entity\Account;
use App\Data\Entity\ApiToken;
use App\Data\Entity\RefreshToken;
use App\Exception\AccessDeniedException;
use App\Exception\BadRequestException;
use App\Exception\DataNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class OAuth2AuthService implements OAuth2ServiceInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var UserPasswordEncoderInterface */
    private $userPasswordEncoder;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordEncoder = $passwordEncoder;
        $this->serializer = $serializer;
    }

    /**
     * Called to get access token and refresh token according to OAuth2.
     * Email as username and password is user to do that.
     *
     * @param array $rawCredentials
     *
     * @return array
     *
     * @throws Exception
     */
    public function auth(array $rawCredentials): array
    {
        $credentials = $this->getCredentials($rawCredentials);

        /** @var Account $user */
        $account = $this->getAccount($credentials);

        $this->checkCredentials($credentials, $account);

        return $this->authenticationSuccess($account);
    }

    /**
     * Return whatever credentials you want to be passed to getUser() as $credentials.
     *
     * @param array $rawCredentials
     *
     * @return array
     *
     * @throws BadRequestException
     */
    public function getCredentials(array $rawCredentials): array
    {
        $missingCredentials = [];

        if (!isset($rawCredentials['username'])) {
            $missingCredentials[] = 'Email not provided';
        }

        if (!isset($rawCredentials['password'])) {
            $missingCredentials[] = 'Password not provided';
        }

        if (!isset($rawCredentials['client_id'])) {
            $missingCredentials[] = 'Client id not provided';
        }

        if (!isset($rawCredentials['grant_type'])) {
            $missingCredentials[] = 'Grant type not provided';
        } elseif ('password' !== $rawCredentials['grant_type']) {
            $missingCredentials[] = 'Grant type should be password';
        }

        if (count($missingCredentials) > 0) {
            throw new BadRequestException($missingCredentials);
        }

        return [
            'email' => $rawCredentials['username'],
            'password' => $rawCredentials['password'],
        ];
    }

    /**
     * @param array $credentials
     *
     * @return Account
     *
     * @throws DataNotFoundException
     */
    public function getAccount(array $credentials): Account
    {
        /** @var Account|null $account */
        $account = $this->entityManager->getRepository(Account::class)
            ->findOneBy(['email' => $credentials['email']]);

        if (null === $account) {
            throw new DataNotFoundException(['Email could not be found.']);
        }

        return $account;
    }

    /**
     * @param array   $credentials
     * @param Account $account
     *
     * @throws AccessDeniedException
     */
    public function checkCredentials(array $credentials, Account $account): void
    {
        if (!$this->userPasswordEncoder->isPasswordValid($account, $credentials['password'])) {
            throw new AccessDeniedException(['Bad credentials.']);
        }
    }

    /**
     * @param Account $account
     *
     * @return array
     *
     * @throws Exception
     */
    public function authenticationSuccess(Account $account): array
    {
        $apiToken = new ApiToken($account);
        $this->entityManager->persist($apiToken);

        $refreshToken = new RefreshToken($account);
        $this->entityManager->persist($refreshToken);
        $this->entityManager->flush();

        /** @var SerializationContext $groups */
        $groups = SerializationContext::create()->setGroups(
            ['Default', 'account_with_contact', 'account_with_personal_info']
        );

        $accountArray = json_decode(
            $this->serializer->serialize(
                $account,
                'json',
                $groups
            ),
            true
        );

        return [
            'access_token' => $apiToken->getToken(),
            'refresh_token' => $refreshToken->getToken(),
            'expires_in' => ApiToken::EXPIRES_IN,
            'token_type' => 'bearer',
            'account' => $accountArray,
        ];
    }
}
