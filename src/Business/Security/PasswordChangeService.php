<?php

declare(strict_types=1);

namespace App\Business\Security;

use App\Data\Entity\Account;
use App\Data\Transformer\AccountTransformer;
use App\Exception\BadRequestException;
use App\Exception\SymfonyValidationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PasswordChangeService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Security */
    private $security;

    /** @var UserPasswordEncoderInterface */
    private $userPasswordEncoder;

    /** @var AccountTransformer */
    private $accountTransformer;

    /** @var ValidatorInterface */
    private $validator;

    /**
     * PasswordForgetService constructor.
     *
     * @param EntityManagerInterface       $entityManager
     * @param Security                     $security
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param AccountTransformer           $accountTransformer
     * @param ValidatorInterface           $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Security $security,
        UserPasswordEncoderInterface $userPasswordEncoder,
        AccountTransformer $accountTransformer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->accountTransformer = $accountTransformer;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     *
     * @throws BadRequestException
     * @throws SymfonyValidationException
     */
    public function change(array $data): void
    {
        $missingValues = [];

        if (!isset($data['old_password'])) {
            $missingValues[] = 'Old password not provided';
        }

        if (!isset($data['new_password'])) {
            $missingValues[] = 'New password not provided';
        }

        if (!isset($data['new_password_verify'])) {
            $missingValues[] = 'New password verify not provided';
        }

        if (count($missingValues) > 0) {
            throw new BadRequestException($missingValues);
        }

        if ($data['new_password'] !== $data['new_password_verify']) {
            throw new BadRequestException(['New password and its verify are not the same.']);
        }

        /** @var Account $account */
        $account = $this->security->getUser();

        if (!$this->userPasswordEncoder->isPasswordValid($account, $data['old_password'])) {
            throw new BadRequestException(['Old password is not right.']);
        }

        $accountModel = $this->accountTransformer->populateModelFromArray(
            ['plain_password' => $data['new_password']]
        );

        $validationErrors = $this->validator->validate($accountModel, null, ['password']);

        if (count($validationErrors) > 0) {
            throw new SymfonyValidationException($validationErrors);
        }

        $this->accountTransformer->populateEntityPassword($accountModel, $account);
        $this->entityManager->merge($account);
        $this->entityManager->flush();
    }
}
