<?php

declare(strict_types=1);

namespace App\Business\Security\Web;

use App\Data\Transformer\AccountTransformer;
use App\Exception\ConflictException;
use App\Exception\SymfonyValidationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WebRegisterService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ValidatorInterface */
    private $validator;

    /** @var AccountTransformer */
    private $accountTransformer;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        AccountTransformer $transformer
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->accountTransformer = $transformer;
    }

    /**
     * @param array $data
     *
     * @throws ConflictException
     * @throws SymfonyValidationException
     * @throws Exception
     */
    public function createAccount(array $data): void
    {
        $accountModel = $this->accountTransformer->populateModelFromArray($data);

        $validationErrors = $this->validator->validate($accountModel, null, ['create']);

        if (count($validationErrors) > 0) {
            throw new SymfonyValidationException($validationErrors);
        }

        try {
            $account = $this->accountTransformer->populateEntityCreate($accountModel);
            $this->entityManager->persist($account);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictException(['Email is already used']);
        }
    }
}
