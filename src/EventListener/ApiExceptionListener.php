<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Exception\BaseException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ApiExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        if ('application/json' !== $event->getRequest()->getContentType()) {
            return;
        }

        $exception = $event->getException();

        $args = [
            'message' => $exception->getMessage(),
        ];

        if ($exception instanceof BaseException) {
            $args['identification'] = $exception->getIdentification();
            $args['errors'] = $exception->getParameters();
            if (in_array($_ENV['APP_ENV'], ['dev', 'test'])) {
                $args['debug'] = $exception->getDebugParameters();
            }

            $responseCode = $exception->getCode();
        }

        $responseCode = $responseCode ?? 500;
        $responseCode = 0 === $responseCode ? 500 : $responseCode;

        $response = new JsonResponse(
            $args,
            $responseCode
        );

        $event->setResponse($response);
    }
}
