<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Exception\BaseException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Twig\Environment;

class WebExceptionListener
{
    const ACCEPTED_CODES = [400, 401, 403, 404, 409];

    /** @var Environment */
    private $twig;

    /**
     * WebExceptionListener constructor.
     *
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        if ('application/json' === $event->getRequest()->getContentType()) {
            return;
        }

        $exception = $event->getException();

        $args = [
            'message' => $exception->getMessage(),
        ];

        if ($exception instanceof BaseException) {
            $args['identification'] = $exception->getIdentification();
            $args['errors'] = $exception->getParameters();
            if (in_array($_ENV['APP_ENV'], ['dev', 'test'])) {
                $args['debug'] = $exception->getDebugParameters();
            }
        }

        $responseCode = $exception->getCode();
        $responseCode = in_array($responseCode, self::ACCEPTED_CODES) ? $responseCode : 500;

        $content = $this->twig->render('web/exception/' . $responseCode . '.html.twig', $args);

        $response = new Response($content, $responseCode);

        $event->setResponse($response);
    }
}
