<?php

declare(strict_types=1);

namespace App\DataFixtures\Test;

use App\Data\Entity\Account;
use App\DataFixtures\TestFixtures;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountFixtures extends TestFixtures
{
    /** @var UserPasswordEncoderInterface */
    private $userPasswordEncoder;

    /**
     * AccountFixtures constructor.
     *
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $account = new Account('janedoe@gmail.com', 'Jane', 'Doe');
        $account->setPassword($this->userPasswordEncoder->encodePassword($account, 'heslo'));

        $manager->persist($account);
        $manager->flush();
    }
}
