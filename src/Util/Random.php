<?php

declare(strict_types=1);

namespace App\Util;

use Exception;

class Random
{
    /**
     * @param int    $length
     * @param string $charset
     *
     * @return string
     *
     * @throws Exception
     */
    public static function string(int $length = 20, string $charset = 'ABCDEFGHIJKLMNOPQRSTUVWYXZ'): string
    {
        $string = '';
        $charsetLength = mb_strlen($charset);

        for ($i = 0; $i < $length; ++$i) {
            $string .= $charset[random_int(0, $charsetLength - 1)];
        }

        return $string;
    }
}
