<?php

declare(strict_types=1);

namespace App\Presentation\Controller\Api\V1;

use App\Business\Security\PasswordChangeService;
use App\Business\Service\AccountService;
use App\Exception\BadRequestException;
use App\Exception\SymfonyValidationException;
use App\Presentation\Controller\Api\BaseApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends BaseApiController
{
    /**
     * @Route("/account/password-change", name="app_api_v1_account_change_password", methods={"PUT"})
     *
     * @param Request               $request
     * @param PasswordChangeService $passwordChangeService
     *
     * @return JsonResponse
     *
     * @throws BadRequestException
     * @throws SymfonyValidationException
     */
    public function passwordChange(Request $request, PasswordChangeService $passwordChangeService): JsonResponse
    {
        /** @var string $content */
        $content = $request->getContent();
        $data = json_decode($content, true);

        $passwordChangeService->change($data);

        return $this->handleResponse(['message' => 'Password was changed.']);
    }

    /**
     * @Route("/account", name="app_api_v1_account_edit", methods={"PUT"})
     *
     * @param Request        $request
     * @param AccountService $accountService
     *
     * @return JsonResponse
     *
     * @throws SymfonyValidationException
     */
    public function edit(Request $request, AccountService $accountService): JsonResponse
    {
        /** @var string $content */
        $content = $request->getContent();
        $data = json_decode($content, true);

        $accountService->edit($data);

        return $this->handleResponse(['message' => 'Account was edited.']);
    }
}
