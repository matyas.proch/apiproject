<?php

declare(strict_types=1);

namespace App\Presentation\Controller\Api\V1\Auth;

use App\Business\Security\OAuth2\OAuth2AuthService;
use App\Business\Security\OAuth2\OAuth2RefreshAuthService;
use App\Business\Security\OAuth2\OAuth2RegisterService;
use App\Business\Security\PasswordForgetService;
use App\Exception\AccessDeniedException;
use App\Exception\BadRequestException;
use App\Exception\ConflictException;
use App\Exception\DataNotFoundException;
use App\Exception\SymfonyValidationException;
use App\Presentation\Controller\Api\BaseApiController;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends BaseApiController
{
    /**
     * @Route("/register", name="app_api_v1_auth_register", methods={"POST"})
     *
     * @param Request               $request
     * @param OAuth2RegisterService $registerService
     *
     * @return JsonResponse
     *
     * @throws ConflictException
     * @throws SymfonyValidationException
     */
    public function register(
        Request $request,
        OAuth2RegisterService $registerService
    ): JsonResponse {
        /** @var string $content */
        $content = $request->getContent();
        $data = json_decode($content, true);

        $tokens = $registerService->createAccount($data);

        return $this->handleResponse($tokens, Response::HTTP_CREATED);
    }

    /**
     * @Route("/login", name="app_api_v1_auth_login", methods={"GET"})
     *
     * @param Request           $request
     * @param OAuth2AuthService $authService
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function login(Request $request, OAuth2AuthService $authService): JsonResponse
    {
        /** @var string $content */
        $content = $request->getContent();
        $data = json_decode($content, true);

        $accessTokens = $authService->auth($data);

        return $this->handleResponse($accessTokens);
    }

    /**
     * @Route("/login/refresh", name="app_api_v1_auth_login_refresh", methods={"GET"})
     *
     * @param Request                  $request
     * @param OAuth2RefreshAuthService $refreshAuthService
     *
     * @return JsonResponse
     *
     * @throws AccessDeniedException
     * @throws BadRequestException
     * @throws DataNotFoundException
     */
    public function loginRefresh(Request $request, OAuth2RefreshAuthService $refreshAuthService): JsonResponse
    {
        /** @var string $content */
        $content = $request->getContent();
        $data = json_decode($content, true);

        $accessTokens = $refreshAuthService->auth($data);

        return $this->handleResponse($accessTokens);
    }

    /**
     * @Route("/password-forget", name="app_api_v1_auth_password_forget", methods={"GET"})
     *
     * @param Request               $request
     * @param PasswordForgetService $passwordForgetService
     *
     * @return JsonResponse
     *
     * @throws DataNotFoundException
     */
    public function passwordForget(Request $request, PasswordForgetService $passwordForgetService): JsonResponse
    {
        /** @var string $content */
        $content = $request->getContent();
        $data = json_decode($content, true);

        $passwordForgetService->sendResetMail($data['email']);

        return $this->handleResponse(['message' => 'Reset email was sent.']);
    }

    /**
     * @Route("/password-set", name="app_api_v1_auth_password_set", methods={"PUT"})
     *
     * @param Request               $request
     * @param PasswordForgetService $passwordForgetService
     *
     * @return JsonResponse
     *
     * @throws BadRequestException
     * @throws ConflictException
     * @throws DataNotFoundException
     * @throws SymfonyValidationException
     */
    public function passwordSet(Request $request, PasswordForgetService $passwordForgetService): JsonResponse
    {
        /** @var string $content */
        $content = $request->getContent();
        $data = json_decode($content, true);

        $passwordForgetService->setPassword($data);

        return $this->handleResponse(['message' => 'Password was set.']);
    }
}
