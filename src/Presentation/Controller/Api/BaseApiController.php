<?php

declare(strict_types=1);

namespace App\Presentation\Controller\Api;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BaseApiController extends AbstractController
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function handleResponse(array $data, $response = Response::HTTP_OK, array $header = [])
    {
        return new JsonResponse($data, $response, $header);
    }

    public function handleEntityResponse($entity, $groups = ['Default'], $response = Response::HTTP_OK, $header = [])
    {
        /** @var SerializationContext $groups */
        $groups = SerializationContext::create()->setGroups($groups);

        return new JsonResponse(
            $this->serializer->serialize($entity, 'json', $groups),
            $response,
            $header,
            true
        );
    }
}
