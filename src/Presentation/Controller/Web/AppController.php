<?php

declare(strict_types=1);

namespace App\Presentation\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    /**
     * @Route("/dashboard", name="app_web_dashboard")
     *
     * @return Response
     */
    public function dashboard(): Response
    {
        return $this->render('web/dashboard.html.twig');
    }
}
