<?php

declare(strict_types=1);

namespace App\Presentation\Controller\Web\Auth;

use App\Business\Security\PasswordForgetService;
use App\Business\Security\Web\WebRegisterService;
use App\Exception\BadRequestException;
use App\Exception\ConflictException;
use App\Exception\DataNotFoundException;
use App\Exception\SymfonyValidationException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    /**
     * @Route("/register", name="app_web_auth_register")
     *
     * @param Request            $request
     * @param WebRegisterService $registerService
     *
     * @return Response
     *
     * @throws Exception
     */
    public function register(
        Request $request,
        WebRegisterService $registerService
    ): Response {
        $data = $request->request->all();
        $formSubmitted = 'submitted' === $request->get('registerForm', false);

        $errors = [];
        if ($formSubmitted) {
            try {
                $registerService->createAccount($data);
            } catch (SymfonyValidationException $e) {
                $errors = $e->getParameters();
            } catch (ConflictException $e) {
                $errors = $e->getParameters();
            }
        }

        $data['errors'] = $errors;
        $data['registerForm'] = $formSubmitted;

        return $this->render('web/auth/register.html.twig', [
            'form' => $data,
        ]);
    }

    /**
     * @Route("/login", name="app_web_auth_login")
     *
     * @param Request             $request
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->isGranted('ROLE_REGISTERED')) {
            return $this->redirectToRoute('app_web_dashboard');
        }

        $data = $request->request->all();

        /** @var Session $session */
        $session = $request->getSession();
        $data['email'] = $session->get('login_email', '');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('web/auth/login.html.twig', [
            'form' => $data,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/password-forget", name="app_web_auth_password_forget")
     *
     * @param Request               $request
     * @param PasswordForgetService $passwordForgetService
     *
     * @return Response
     *
     * @throws Exception
     */
    public function passwordForget(Request $request, PasswordForgetService $passwordForgetService): Response
    {
        $data = $request->request->all();
        $formSubmitted = 'submitted' === $request->get('passwordForgetForm', false);

        $errors = [];
        if ($formSubmitted) {
            try {
                $passwordForgetService->sendResetMail($data['email'] ?? null);
            } catch (DataNotFoundException $e) {
                $errors = $e->getParameters();
            }
        }

        $data['errors'] = $errors;
        $data['passwordForgetForm'] = $formSubmitted;

        return $this->render('web/auth/password_forget.html.twig', [
            'form' => $data,
        ]);
    }

    /**
     * @Route("/password-set/{hash}", name="app_web_auth_password_set")
     *
     * @param string                $hash
     * @param Request               $request
     * @param PasswordForgetService $passwordForgetService
     *
     * @return Response
     *
     * @throws BadRequestException
     */
    public function passwordSet(
        string $hash,
        Request $request,
        PasswordForgetService $passwordForgetService
    ): Response {
        $data = $request->request->all();
        $data['hash'] = $hash;
        $formSubmitted = 'submitted' === $request->get('passwordSetForm', false);

        $errors = [];
        if ($formSubmitted) {
            try {
                $passwordForgetService->setPassword($data);
            } catch (DataNotFoundException $e) {
                $errors = $e->getParameters();
            } catch (ConflictException $e) {
                $errors = $e->getParameters();
            } catch (SymfonyValidationException $e) {
                $errors = $e->getParameters();
            }
        }

        $data['errors'] = $errors;
        $data['passwordSetForm'] = $formSubmitted;

        return $this->render('web/auth/password_set.html.twig', [
            'form' => $data,
        ]);
    }
}
